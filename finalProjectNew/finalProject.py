# Name: Joey Carberry
# Date: October 15, 2016
# Project: Game

import random
import time
userClass = 0
userClassText = 'Null'
baseDamage = 2
magicDamage = 0
attackSpeed = 1
armor = 2
health = 100
eDefeated = 0
item = 0
errorCount = 0
speed = 1

class bcolors:
    # Class used to change the color in texts when the game runs.
    # Blue is any inputs that the player need to do
    # Red is any important program messages
    # Purple is the names of Enemies
    bold = '\033[1m'
    end = '\033[0m'
    # Colors
    brightWhite = '\033[30m'
    red = '\033[31m'
    greenYellow = '\033[32m'
    lightOrangeYellow = '\033[33m'
    blue = '\033[34m'
    purple = '\033[35m'
    teal = '\033[36m'
    lightGrey = '\033[37m'
    whiteGrey = '\033[38m'


def mainMenu():
    global speed
    # Main menu for the game, with some information, settings, help, and an option to start the game.
    print('=== Cave Adventure by ' + bcolors.red + 'Cranberry ' + bcolors.end + '===')
    print('Type "Start" to start game.')
    print('Type "Options" for options for game speed.')
    print('Type "Help" for help with the game.')
    print('Type "Credits" for the list of contributers')
    menuCh = input()
    if menuCh == 'Start' or menuCh == 'start' or menuCh == 's' or menuCh == 'S' or menuCh == 'sta':
        startGame()
    if menuCh == 'Options' or menuCh == 'options' or menuCh == 'O' or menuCh == 'o' or menuCh == 'Opt':
        print('Set the game speed.')
        print('1.) Normal')
        print('2.) Fast')
        print('3.) Fastest')
        speed = int(input(bcolors.blue + 'Select speed: ' + bcolors.end))
        print('Your speed has been set to', speed)
        time.sleep(1)
        if speed == 3 or speed == 2:
            print(bcolors.red + 'Note: Speed settings may not work for some sections.' + bcolors.end + '\n')
            time.sleep(.5)
        print()
        mainMenu()
    if menuCh == 'Credits' or menuCh == 'credits' or menuCh == 'C' or menuCh == 'c' or menuCh == 'cre':
        credits()
    else:
        runHelp()


def runHelp():
    # Help section, selected from the main menu, shows information and interesting things.
    print('==== Help ====')
    print('Type "General" for general help with the game.')
    print('Type "Cards" for help with the card selection.')
    print('Type "Stats" for help with stats.')
    print('Type "Fighting" for help with fighting.')
    print('Type "Menu" to go back to the main menu.')
    helpCh = input()
    if helpCh == 'General' or helpCh == 'general' or  helpCh == 'G' or  helpCh == 'general' or helpCh == 'Gen':
        print('==== General Help ====')
        print('Cave Adventure is a fairly simple game.')
        print('There are several "main" mechanisms.')
        print('Such as card selection and fighting.')
        print('(Help is avaiable for both of these in the main options menu)')
        print('When text is ' + bcolors.blue + 'blue ' + bcolors.end + 'that means that it is an entry.')
        print('The player interacts with the game through these entries.')
        print('A choice is given, labled by number. For example:')
        print('1.) Option One')
        print('2.) Option Two')
        fakeChoice = input(bcolors.blue + 'This would be an entry' + bcolors.end)
        if fakeChoice == 1:
            print('You have selected Option One.')
        if fakeChoice == 2:
            print('You have selected Option Two.')
        else:
            print('You did not select an availible answer.')
        print('This has been an overview of the General Selection Process.')
        time.sleep(2.5)
        runHelp()
    if helpCh == 'Cards' or helpCh == 'cards' or helpCh == 'C' or helpCh == 'c' or helpCh == 'Car' or helpCh == 'car':
        print('Cards Selection is how a character\'s starting stats will be chosen.')
        print('Each card belongs to a "mana pool."')
        print('There are 5 mana pools, which two cards in each.')
        print('Such as, Mana Pool 4 has cards 7 and 8.' + '\n')
        time.sleep(5)
        print('At the start of the game you have 8 mana.')
        print('Cards cost mana. Their cost is equal to their mana pool.')
        print('This means a card in Mana Pool 3 would cost 3 mana, leaving you with 5 mana.')
        print('(8 Starting Mana - 3 Mana Cost = 5 Mana Left)' + '\n')
        time.sleep(5)
        print('Cards can be selected multiple times, as long as you do not exceed the 8 mana cost.')
        print('An error will occur if you attempt to use more than 8 mana.')
        continueInput = input(bcolors.blue + 'Continue?' + bcolors.end)
    if helpCh == 'Stats' or helpCh == 'stats' or  helpCh == 'S' or  helpCh == 's' or  helpCh == 'Sta' or helpCh == 'sta':
        print('Stats are how the game handles fights.')
        print('There are several stats:')
        print('Base Damage - Your normal damage, with no class benifits.')
        print('Magic Damage - Damage with a class benifit from Mages.')
        print('Attack Speed - Damage multiplier, with a class benifit from Hunter.')
        print('Armor - Protection, with a class benifit from Warriors.')
        print('Health - Your health')
        print('Resistance - An Enemy specific stat, works the same as Armor.' + '\n')
        time.sleep(5)
        print('These stats combine into "Attack Damage" which is how much damage you deal to an opponent.')
        print('This is calculated by:')
        print('(Base Damage + Magic Damage) * Attack Speed - Armor of Opponent - Resistance of Opponent')
        print('Damage taken from enemies is calculated the same way.' + '\n')
        time.sleep(5)
        print('Each class has benifits for specific stats:')
        print('Warrior - Gets +1 Armor from an Armor Card.')
        print('Mage - Gets a +1 Magic Damage from a Magic Damage Card.')
        print('Hunter - Gets a *2 Attack Speed from an Attack Speed Card.')
        continueInput = input(bcolors.blue + 'Continue?' + bcolors.end)
    if helpCh == 'Fighting' or helpCh == 'fighting' or  helpCh == 'F' or  helpCh == 'f' or  helpCh == 'Fig':
        print('Fighting is a major mechanic of the game.')
        print('Fights take place in 3 stages.')
        print('Pre-Fight')
        print('Active Fight Loop')
        print('Post-Fight' + '\n')
        time.sleep(3)
        print('=== Pre-Fight ===')
        print('This stage gives the player a variety of choices which can hurt of help them.')
        print('Such as looking around for items, or how to approach a fight.')
        print('Often these choices can affect the health, and sometimes stats of the player and enemy.' + '\n')
        time.sleep(5)
        print('=== Active Fight Loop ===')
        print('This stage is a loop that does not end till the player or enemy has zero health.')
        print('Often the player is given a few options over and over again.' + '\n')
        time.sleep(5)
        print('=== Post-Fight ===')
        print('This stage begins when the active loop has ended.')
        print('There are two options which can happen: You die and are prompted to retry, or your enemy dies.')
        print('When your enemy dies, you are given a choice of small bonuses.')
        print('These bonuses get better as the fights ramp up in difficulty.')
        continueInput = input(bcolors.blue + 'Continue?' + bcolors.end)
    else:
        mainMenu()





def credits():
    # Runs the credits, selected from the main menu.
    creditsList = ['Creator, Director, Programmer, Designer, Playtester, Author',
                   bcolors.red + '                                                             Cranberry' + bcolors.end  ,
                   'Playtesters',
                   '                                                               Josh C.',
                   '                                                               Liam W.',
                   '                                                             Peyton O.',
                   '                                                             Rachel C.',
                   '                                                                Kai E.',
                   '                                                             Robbie F.',
                   bcolors.purple + 'Overlord' + bcolors.end,
                   '                                                                Meg P.']
    for x in range(0, len(creditsList)):
        print(creditsList[x])
        time.sleep(1)
    mainMenu()


def startGame():
    global userName
    global speed
    print('Speed: ', speed)
    # Starts the game by introducing the game, creator, and basic story. Prompts the user to enter a name which will be
    # used throughout the game.
    startGameTextList = ['Begin your adventure, a story of love, action, death, and romance.',
                         'Choose your character wisely, for once you die, that is the end.']
    startGameTimeList = [2, 2.5, 2.5]
    startGameTimeListFast = [1, 1.5, 1.5]
    startGameTimeListFastest = [.5, 1.0, 1.0]
    for a in range(0,2):
        print(startGameTextList[a])
        if speed == 1:
            time.sleep(startGameTimeList[a])
        if speed == 2:
            time.sleep(startGameTimeListFast[a])
        if speed == 3:
            time.sleep(startGameTimeListFastest[a])

    userName = input(bcolors.blue + 'What is your name? ' + bcolors.end)
    classSelect()


def classSelect():
    global userClass
    global userClass
    global userClassText
    global userName
    global weapon
    global speed
    global errorCount
    # DevCheat Shortcut
    if userName == 'devCheat' or userName == 'skip':
        userClass = 2
        userClassText = 'Mage'
        weapon = 'staff'
        cardSelect()
    # Prompts the now-named user to select a class, explaining that each class has buffs and so on.
    classSelectTextList = ['You must select your class. Each class has its unique benefits.',
                           'The Warrior class has strong armor, and can defend from powerful attacks.',
                           'The Mage class uses magics to deal extra damage.',
                           'The Hunter class is swift and attacks faster dealing more damage.']
    classSelectTimeList = [3, 3, 2.5, 2.5]
    classSelectTimeListFast = [2, 2, 1.5, 1.5]
    classSelectTimeListFastest = [1, 1, .5, .5]
    for a in range(0, 4):
        print(classSelectTextList[a])
        if speed == 1:
            time.sleep(classSelectTimeList[a])
        if speed == 2:
            time.sleep(classSelectTimeListFast[a])
        if speed == 3:
            time.sleep(classSelectTimeListFastest[a])
    # userClass inputs can use both lower and upper case. Such as Warrior and warrior.
    userClassInput = input(bcolors.blue + 'Which class shall you be? ' + bcolors.end)
    # Creates a user variable of the class so it can modified later
    if userClassInput == 'Warrior' or userClassInput == 'warrior' or userClassInput == 'w' or userClassInput == 'W':
        userClass = 1
        userClassText = 'Warrior'
        weapon = 'axe'
    elif userClassInput == 'Mage' or userClassInput == 'mage' or userClassInput == 'm' or userClassInput == 'M':
        userClass = 2
        userClassText = 'Mage'
        weapon = 'staff'
    elif userClassInput == 'Hunter' or userClassInput == 'hunter' or userClassInput == 'h' or userClassInput == 'H':
        userClass = 3
        userClassText = 'Hunter'
        weapon = 'bow'
    elif errorCount >= 3:
        print('Having some trouble?')
        time.sleep(2)
        print('When asked what class you want to be, type either:')
        time.sleep(2)
        print('"Warrior"')
        time.sleep(2)
        print('"Mage"')
        time.sleep(2)
        print('"Hunter"')
        time.sleep(2)
        print('Please try again.')
        time.sleep(1.5)
        classSelect()

    else:
        print('I\'m sorry, that\'s not a class.')
        time.sleep(2)
        print('Please try again.')
        time.sleep(1.5)
        errorCount += 1
        classSelect()

    time.sleep(1)
    print('Welcome,', userName, 'the', userClassText)
    time.sleep(1.5)
    townStart()

def townStart():
    global userName
    global userClassText
    global item
    global speed
    if speed == 1:
        tSpeed = 1.5
    if speed == 2:
        tSpeed = 1
    if speed == 3:
        tSpeed = .5
    helloString = '"' + userName
    print(helloString, 'is that you?"')
    time.sleep(tSpeed + .5)
    print('You turn around, to see your good friend, Greg, a fellow', userClassText, 'walking towards you.')
    time.sleep(tSpeed + 1.5)
    townSTextList1 = ['"Oh thank the Almighty Spider Lord! I thought you had already left!"',
                      '"Before you go into that wretched cave, I must give you something!"',
                      '"I don\'t have much... But I can give you a choice!"',
                      '1.) Cloak of Wisdom',
                      '2.) Elixir of Health',
                      '3.) Some extra deodorant',
                      '4.) You\'re not some charity case!']
    townSTimeList1 = [2.5, 2.5, 2, 1, 1, 1, 1]
    townSTimeList1Fast = [1.5, 1.5, .1, .5, .5, .5, .5]
    townSTimeList1Fastest = [1, .75, .5, .5, .5, .5, .5]
    for a in range(0, 7):
        print(townSTextList1[a])
        if speed == 1:
            time.sleep(townSTimeList1[a])
        if speed == 2:
            time.sleep(townSTimeList1Fast[a])
        if speed == 3:
            time.sleep(townSTimeList1Fastest[a])
    userChoice1 = int(input(bcolors.blue + 'What would you like to take with you? '+ bcolors.end))
    time.sleep(1)
    if userChoice1 == 1:
        item = 1
        print('You choose to take the Cloak of Wisdom.')
        time.sleep(tSpeed + .75)
        print('You drape it over yourself, and feel a strong energy...')
        time.sleep(tSpeed + 1)
    if userChoice1 == 2:
        item = 2
        print('You pocket the Elixir of Health, potions such as these are very powerful...')
        time.sleep(tSpeed + 2)
    if userChoice1 == 3:
        item = 3
        print('You pocket the extra deodorant, it is some Old Spice SpiderFang.')
        time.sleep(tSpeed + 1.5)
        print('You are quite happy with your choice.')
        time.sleep(tSpeed + 1)
    if userChoice1 == 4:
        print('"Wow way to be rude', userName, '!"')
        time.sleep(tSpeed + .5)
        print('Greg stomps off, you feel a little sad...')
        time.sleep(tSpeed + 1)
    print('You leave Greg and continue down the path, leading to the cave...')
    time.sleep(tSpeed + 1.5)
    print('But before your enter the cave, you must choose some cards.')
    time.sleep(tSpeed + 1.25)
    cardSelect()

def cardSelect():
    # Generates randoms numbers for each mana pool. Displays two cards in each mana pool as a card, ten in total,
    # labeled 1-10. Player is prompted to choose cards by entering numbers. Character has only 6 mana, and cannot choose
    # a combination of cards costing over 6 mana. Once final cards are chosen, the player stats are updated.
    global baseDamage
    global magicDamage
    global attackSpeed
    global armor
    global userName
    global speed
    if userName == 'devCheat' or userName == 'skip':
        baseDamage += 10
        magicDamage += 5
        attackSpeed += .5
        armor += 5
        displayStats()
        enterCave()
    manaPoolSizeList = [8, 12, 15, 16, 15]
    # Defines the location of the card files
    cards1 = 'cards.txt'
    cards2 = 'cards2.txt'
    cards3 = 'cards3.txt'
    cards4 = 'cards4.txt'
    cards5 = 'cards5.txt'
    cardsFileList = (cards1, cards2, cards3, cards4, cards5)
    manaPool = 1
    cardNumber = 1
    cardRandomChoiceList = []
    cardMasterList = []
    baseDamageList = []
    magicDamageList = []
    attackSpeedList = []
    armorList = []
    remainingMana = 8
    time.sleep(2)
    print('These cards improve your character\'s might in battle. Every card cost mana, equal to what pool it is in.')
    if speed == 1:
        time.sleep(4)
    if speed == 2:
        time.sleep(2.5)
    if speed == 3:
        time.sleep(1.5)
    print('The cards you choose cannot cost more than 8 mana in total.')
    if speed == 1:
        time.sleep(3)
    if speed == 2:
        time.sleep(2)
    if speed == 3:
        time.sleep(1)

    # Displays the bonuses of the user's class
    if (userClass == 1):
        print('Your class, the', userClassText, 'has a + 1 Armor bonus for each card with an Armor buff.')
    if (userClass == 2):
        print('Your class, the', userClassText, 'has a + 1 Magic Damage bonus for each card with a Magic Damage buff.')
    if (userClass == 3):
        print ('Your class, the', userClassText, 'doubles the Attack Speed buff of any card with an Attack Speed buff.')
    time.sleep(2)

    # Prints the card texts
    for x in range(0,5):
        print('Mana Pool: ', manaPool)
        cardsFile = open(cardsFileList[x], 'r')
        cardsFileLine = cardsFile.readlines()

        # Creates two random numbers which refer to a line in the cards file to display as the buff
        randomCardChoice1 = random.randint(0,manaPoolSizeList[x])
        randomCardChoice2 = random.randint(0,manaPoolSizeList[x])

        # Checks the random numbers to adjust them to find the correct lines in the file
        if (randomCardChoice1 > 0):
            randomCardChoice1 = (randomCardChoice1 - 1) * 5
        if (randomCardChoice2 > 0):
            randomCardChoice2 = (randomCardChoice2 - 1) * 5

        # Finds the length of the chosen line in the file
        cardsFileLine1 = cardsFileLine[randomCardChoice1]
        cardsFileLineLength1 = cardsFileLine1.__len__() - 1
        cardsFileLine2 = cardsFileLine[randomCardChoice2]
        cardsFileLineLength2 = cardsFileLine2.__len__() - 1

        # Adds the randomCardChoice to a list be later used to find the stats under it.
        cardRandomChoiceList.append(randomCardChoice1)
        cardRandomChoiceList.append(randomCardChoice2)
        # Prints the number of the card and the card text.
        print(cardNumber, '.)', cardsFileLine1[0:cardsFileLineLength1])
        cardNumber += 1
        print(cardNumber, '.)', cardsFileLine2[0:cardsFileLineLength2])
        cardNumber += 1
        manaPool += 1
        time.sleep(.5)
        # Adds the card stats to a 'master list' of stats
        for y in range(0,4):
            randomCardChoice1 += 1
            cardsValueFileLine1 = cardsFileLine[randomCardChoice1]
            cardsValueFileLineLength1 = cardsValueFileLine1.__len__() - 1
            cardListValue = int(cardsValueFileLine1[0:cardsValueFileLineLength1])
            cardMasterList.append(cardListValue)
        for z in range(0,4):
            randomCardChoice2 += 1
            cardsValueFileLine2 = cardsFileLine[randomCardChoice2]
            cardsValueFileLineLength2 = cardsValueFileLine2.__len__() - 1
            cardListValue = int(cardsValueFileLine2[0:cardsValueFileLineLength2])
            cardMasterList.append(cardListValue)

    # Prompts the user for a card choice. The total mana cost must be =< 8 mana. The user can select as many cards as
    # they would like, but cannot choose the cards more than once.
    cardChoice = True
    while cardChoice:
        if (remainingMana <= 0):
            cardChoice = False
            print('No mana left.')
        if (cardChoice == True):
            userCardInputPrompt = 'What card do you want for your ' + userClassText + '? '
            userCardInput = int(input(bcolors.blue + userCardInputPrompt + bcolors.end))
            if(findManaPool(userCardInput) > remainingMana):
                print('Nice try. Attempting to use more mana than you have. You are punished with life in purgatory.')
                cardChoice = False
                death = True
                while death:
                    for z in range(0,100000000):
                        time.sleep(10)
                        print('Memes')
                    death = False
            if (cardChoice == True):
                # Adds the correct stats from the individual lists for each stat.
                # Base Damage
                cardBaseDamageListValue = (userCardInput * 4) - 4
                userCardChoiceBaseDamage = cardMasterList[cardBaseDamageListValue]
                baseDamageList.append(userCardChoiceBaseDamage)
                # Magic Damage
                cardMagicDamageListValue = (userCardInput * 4) - 3
                userCardChoiceMagicDamage = cardMasterList[cardMagicDamageListValue]
                magicDamageList.append(userCardChoiceMagicDamage)
                # Attack Speed
                cardAttackSpeedListValue = (userCardInput * 4) - 2
                userCardChoiceAttackSpeed = cardMasterList[cardAttackSpeedListValue]
                attackSpeedList.append(userCardChoiceAttackSpeed)
                # Armor
                cardArmorListValue = (userCardInput * 4) - 1
                userCardChoiceArmor = cardMasterList[cardArmorListValue]
                armorList.append(userCardChoiceArmor)
                remainingMana -= findManaPool(userCardInput)
                print('You have ', remainingMana, 'mana left.')
                if (remainingMana <= 0):
                    cardChoice = False
        # Adds the class bonus to the stats
        if (cardChoice == False):
            baseDamage += sum(baseDamageList)
            magicDamage += sum(magicDamageList)
            attackSpeedListCorrectedSum = sum(attackSpeedList) / 100
            attackSpeed += attackSpeedListCorrectedSum
            armor += sum(armorList)
        if (cardChoice == False):
            if (userClass == 1):
                armor += len(armorList)
            if (userClass == 2):
                magicDamage += len(magicDamageList)
            if (userClass == 3):
                attackSpeed += attackSpeedListCorrectedSum
    displayStats()
    enterCave()


def findManaPool(userCardInput):
    # Small function that is used to calculate what mana pool a card is in
    if (userCardInput == 1 or userCardInput == 2):
        answer = 1
    if (userCardInput == 3 or userCardInput == 4):
        answer = 2
    if (userCardInput == 5 or userCardInput == 6):
        answer = 3
    if (userCardInput == 7 or userCardInput == 8):
        answer = 4
    if (userCardInput == 9 or userCardInput == 10):
        answer = 5
    return(answer)

def displayStats():
    global baseDamage
    global magicDamage
    global attackSpeed
    global armor
    global userName
    # A function which shows the user what their stats are, including the class buffs.
    print(bcolors.bold + userName, 'stats' + bcolors.end)
    print('=======================')
    print('Base Damage: ', int(baseDamage))
    print('Magic Damage: ', magicDamage)
    print('Attack Speed: ', attackSpeed)
    print('Armor: ', armor)
    print('=======================')
    # Displays small flavor text if a character has high stats
    if (baseDamage > 30):
        print('You must be a heavy hitter with that base damage!')
    if (armor > 40):
        print('That\'s a lot of armor!')
    if (magicDamage > 30):
        print('That\'s a lot of magic damage!')
    if (attackSpeed > 3):
        print('You must be quick with that attack speed!')


def enterCave():
    # Beginning of the real game after the user has chosen name, class, and cards. Telling the story
    global userName
    global weapon
    global speed
    global item
    if userName == 'devCheat' or userName == 'skip':
        molluskFight()
    for a in range(0,11):
        print()
        if a == 5:
            print('YOU ENTER THE CAVE')
        if speed == 1:
            time.sleep(1)
        if speed == 2:
            time.sleep(.5)
        if speed == 3:
            time.sleep(.25)
    if item == 1:
        print('Your cloak of wisdom starts to glow...')
        time.sleep(1.5)
        print('You hear a light voice in your head...')
        time.sleep(1.5)
        print('"This was probably not a good idea to be honest"')
        time.sleep(2)
    if item == 2:
        print('You decide to drink the elixir now, just in case...')
        time.sleep(2)
        print('You gain 2 Health, must have been a knock-off brand...')
        time.sleep(2)
    if item == 3:
        print('You apply the deodorant to your armpits.')
        time.sleep(1.5)
        print('You now was a very fresh scent.')
        time.sleep(1.5)
    enemyList = ['It is the legendary', 'GIANT', 'MAN-EATING', 'HORRIBLE', 'GRUESOME', 'HORRIFYING', 'GIANT',
                 'ABNORMALLY LARGE', 'TERRIBLE', 'SPOOKY', 'SCARY', 'FURIOUS', 'MOLLUSK!']
    enemyListLength = len(enemyList)
    print('Here your adventure begins. You will face a variety of enemies, and maybe a few friends.')
    time.sleep(2)

    enterCaveTextList1 = ['You enter the cave, holding a lantern in front of your face, dispelling the darkness.',
                         'Mysterious noises are heard from the depths.',
                         'To your left, you hear a scuffling...',
                         'You turn to face the beast!']
    enterCaveTimeList1 = [2.5, 2.5, 2, 2]
    enterCaveTimeList1Fast = [1.5, 1.5, 1, 1]
    enterCaveTimeList1Fastest = [.75, .75, .5, .5]
    for b in range(0, 4):
        print(enterCaveTextList1[b])
        if speed == 1:
            time.sleep(enterCaveTimeList1[b])
        if speed == 2:
            time.sleep(enterCaveTimeList1Fast[b])
        if speed == 3:
            time.sleep(enterCaveTimeList1Fastest[b])
        if b == 1:
            print('Your knuckles are white as your grip your', weapon, ', preparing for the unknown.')
            if speed == 1:
                time.sleep(2.5)
            if speed == 2:
                time.sleep(1.5)
            if speed == 3:
                time.sleep(1)
    for c in range(0, enemyListLength):
        print(enemyList[c])
        if speed == 1:
            time.sleep(1.5)
        if speed == 2:
            time.sleep(.75)
        if speed == 3:
            time.sleep(.25)
    molluskFight()


def molluskFight():
    # First fight with the Mollusk, meant to be comical and give a basic understanding of teh game for the user.
    global baseDamage
    global magicDamage
    global attackSpeed
    global armor
    global userClass
    global userName
    global eDefeated
    global weapon
    global speed
    if userName == 'devCheat' or userName == 'skip':
        baseDamage += 1
        jorgonFight()
    attackDamage = (baseDamage + magicDamage) * attackSpeed
    molluskHealth = 50
    molluskFighting = True
    # List containing the story text for the fight
    print(bcolors.purple + 'The Giant Mollusk' + bcolors.end)
    print('=======================')
    print('Base Damage: 0')
    print('Attack Speed: 0')
    print('Armor: 0')
    print('Resistance: 0')
    print('Health: 50')
    print('=======================')
    molluskFightTextList1 = ['You face the mollusk, horrified by it\'s might!',
                            'It\'s at least the size of your hand, maybe even bigger.',
                            'It could probably give you a pretty bad rash or something, who knows?',
                            'You prepare to battle!',
                            'It seems to fight back! Rocking back and forth slightly.',
                            'You take no damage', 'You attack again!']
    time.sleep(1)
    for a in range(0,4):
        print(molluskFightTextList1[a])
        if speed == 1:
            time.sleep(2)
        if speed == 2:
            time.sleep(1)
        if speed == 3:
            time.sleep(.5)

    print('Your attack damage is ', attackDamage)
    time.sleep(1)
    # Begin Battle
    if speed == 1:
        tSpeed = 1.25
    if speed == 2:
        tSpeed = .75
    if speed == 3:
        tSpeed = .25
    print('What would you like to do to combat the mollusk?')
    time.sleep(tSpeed)
    print('1.) Look around for a proper weapon!')
    time.sleep(tSpeed)
    print('2.) Retreat!')
    time.sleep(tSpeed)
    print('3.) Attack the Mollusk with all your might!')
    time.sleep(tSpeed)
    uMolluskChoice = int(input(bcolors.blue + 'What shall you do? ' + bcolors.end))

    if uMolluskChoice == 1:
        time.sleep(2)
        print('You look around for a weapon in the darkness...')
        if (userClass == 1):
            time.sleep(2.5)
            print('You find a Fiery War Axe!')
            baseDamage += 3
            time.sleep(2)
            print('This axe will make your attacks more powerful!')
            time.sleep(1)
        if (userClass == 2):
            time.sleep(2.5)
            print('You find a magical staff!')
            magicDamage += 3
            time.sleep(2)
            print('This staff will make your spells more powerful!')
            time.sleep(1)
        if (userClass == 3):
            time.sleep(2.5)
            print('You find a mighty Eaglehorn Bow!')
            baseDamage += 3
            time.sleep(2)
            print('This bow will deal extra damage!')
            time.sleep(1)
        print('What would you like to do to combat the mollusk?')
        time.sleep(tSpeed)
        print('1.) Retreat!')
        time.sleep(tSpeed)
        print('2.) Attack the Mollusk with all your might!')
        time.sleep(tSpeed)
        uMolluskChoice2 = int(input(bcolors.blue + 'What shall you do? '+ bcolors.end))
        if uMolluskChoice2 == 1:
            uMolluskChoice = 2

    time.sleep(2)
    if uMolluskChoice == 2:
        beginFight = True
        while beginFight:
            print('You decide to run home to your mother.')
            time.sleep(2)
            print('A cowardly warrior is never rewarded.')
            time.sleep(2)
            print('You run away from the Mollusk.')
            time.sleep(2)
            print('You trip and fall into a pit of snakes.')
            for x in range(0, 999999999):
                print('snuke')
                time.sleep(60)
            endGame()
    if (userClass == 1):
        print('You swing your mighty axe at the Mollusk!')
    if (userClass == 2):
        print('You use a powerful spell against the Mollusk!')
    if (userClass == 3):
        print('You fire an arrow at the Mollusk')
    time.sleep(1)
    molluskHealth -= attackDamage
    print('The mollusk is now damaged with ', molluskHealth, 'health!')
    molluskHealth -= attackDamage
    for x in range(4,7):
        print(molluskFightTextList1[x])
        time.sleep(2)
    # Continues to attack while the mollusk is still alive.
    while molluskFighting:
        time.sleep(2)
        print('And again...')
        molluskHealth -= attackDamage
        if (molluskHealth <= 0):
            molluskFighting = False
    time.sleep(2)
    print('The mollusk has been slain!')
    eDefeated += 1
    # After the Mollusk has been slain the user can get a bonus.
    time.sleep(tSpeed)
    print('Every time you defeat an enemy, you gain a bonus! Choose your bonus!')
    time.sleep(tSpeed)
    print('1.) + 1 Base Damage')
    time.sleep(tSpeed)
    print('2.) + 2 Armor')
    time.sleep(tSpeed)
    print('3.) + 1 Magic Damage')
    time.sleep(tSpeed)
    userMolluskBonus = int(input(bcolors.blue + 'Which bonus do you want? '+ bcolors.end))
    # Adds the bonus
    if userMolluskBonus == 1:
        print('You now have + 1 Base Damage!')
        baseDamage += 1
    if userMolluskBonus == 2:
        print('You now have + 2 Armor!')
        armor += 2
        if userClass == 1:
            print('Class bonus has been applied.')
            armor += 1
    if userMolluskBonus == 3:
        print('You now have + 1 Magic Damage!')
        magicDamage += 1
        if userClass == 2:
            print('Class Bonus has been applied')
            magicDamage += 1
    displayStats()
    jorgonFight()

def jorgonFight():
    # Second scripted fight, with Jorgon the Dog-Mouthed. He is the first enemy able to do damage.
    global baseDamage
    global magicDamage
    global attackSpeed
    global armor
    global health
    global userName
    global eDefeated
    global weapon
    global speed
    # The stats for Jorgon the Dog-Mouthed
    if userName == 'devCheat' or userName == 'skip':
        luumFight()

    jorgonBaseDamage = 20
    jorgonAttackSpeed = 1
    jorgonArmor = 1
    jorgonResist = 1
    jorgonHealth = 30
    jorgonLive = True
    jorgonAttackDamage = (jorgonBaseDamage)*(jorgonAttackSpeed) - armor
    if jorgonAttackDamage < 0:
        jorgonAttackDamage = 0
    attackDamage = ((baseDamage+magicDamage) * attackSpeed) - jorgonArmor - jorgonResist
    jFTextList1 = ['You stumble away from the mollusk, exhausted from the horrid battle.',
                           'But you are safe, there is nothing to fear now...', 'Except there is!',
                           'Out of the darkness you hear a long howl.', 'You spot a dim light ahead of you.',
                           'You consider what to do...', '1.) Walk towards the light quietly.',
                           '2.) Rush towards the light fiercely.',
                           '3.) Investigate your surroundings for something useful.']
    jFTimeList1 = [2, 2.5, 1.5, 2.5, 2.5, 1.5, 1.5, 1.5, 1.5, 1.5]
    jFTimeList1Fast = [1, 1.5, .75, 1.5, 1.5, .75, .75, .75, .75, .75]
    jFTimeList1Fastest = [.5, .75, .5, .75, .75, .5, .5, .5, .5, .5]
    if speed == 1:
        tSpeed = 1.5
    if speed == 2:
        tSpeed = 1
    if speed == 3:
        tSpeed = .75
    for a in range(0,9):
        print(jFTextList1[a])
        if speed == 1:
            time.sleep(jFTimeList1[a])
        if speed == 2:
            time.sleep(jFTimeList1Fast[a])
        if speed == 3:
            time.sleep(jFTimeList1Fastest[a])
    userChoice = int(input(bcolors.blue + 'What would you like to do? ' + bcolors.end))
    if userChoice == 1:
        jFTextList2 = ['You creep along the cave\'s edge slowly.', 'Your heart beats quickly.',
                                'You crouch behind a large rock close to the source of the light.',
                                'You consider what to do...', '1.) Peer over the rock...',
                                '2.) Launch yourself over the rock, and attack anything that may face you.']
        jFTimeList2 = [2, 2, 3.5, 1.5, 1.5, 1.5]
        jFTimeList2Fast = [1, 1, 2.5, .75, .75, .75]
        jFTimeList2Fastest = [.5, .5, 1, .5, .5, .5]
        for b in range(0,6):
            print(jFTextList2[b])
            if speed == 1:
                time.sleep(jFTimeList2[b])
            if speed == 2:
                time.sleep(jFTimeList2Fast[b])
            if speed == 3:
                time.sleep(jFTimeList2Fastest[b])
        userChoice2 = int(input(bcolors.blue + 'What would you like to do? '+ bcolors.end))
        if userChoice2 == 1:
            print('You peer over the rock...')
            time.sleep(tSpeed)
        if userChoice2 == 2:
            print('You launch yourself over the rock wielding your', weapon, ', screaming.')
            time.sleep(tSpeed + 2)
    if userChoice == 2:
        print('You rush along the cave tunnel towards the light.')
        time.sleep(tSpeed + 1)
        print('Ahead, you see a fire, with nobody around...')
        time.sleep(tSpeed)
        print('You hear a shuffling ahead...')
        time.sleep(tSpeed)
        print('You grip your', weapon, '.')
        time.sleep(tSpeed)
    if userChoice == 3:
        jFTextList3 = ['You decide to investigate the rocks around you.',
                       'Being mindful not to make too much noise, you overturn rocks looking for something useful.',
                       'You overturn a boulder, finding a small vial of red liquid...', 'You consider what to do...',
                       '1.) Drink the vial.', '2.) Leave the vial.',
                       'with the red liquid.']
        jFTimeList3 = [3, 4.5, 2, 1.5, 1.5, 1.5, 1.5]
        jFTimeList3Fast = [2, 3.5, 1, .75, .75, .75]
        jFTimeList3Fastest = [1, 2, .5, .5, .5, .5]
        for c in range(0, 6):
            print(jFTextList3[c])
            if speed == 1:
                time.sleep(jFTimeList3[c])
            if speed == 2:
                time.sleep(jFTimeList3Fast[c])
            if speed == 3:
                time.sleep(jFTimeList3Fastest[c])
            if c == 5:
                print('3.) Dip the end of your', weapon, 'in the vial.')
                time.sleep(1)
        userChoice3 = int(input(bcolors.blue + 'What shall you do? ' + bcolors.end))
        time.sleep(2)
        jFTextList4 = ['You feel heat all over your body, rushing through you.',
                       'You feel stronger than ever!', 'You have gained 10 health!',
                       'Emboldened by your new strength you rush towards the light!',

                       'You leave the vial and continue towards the light cautiously.',
                       'You near the light, and crouch behind a large rock.',
                       'You peer over slowly...',

                       'You dip your weapon with the red liquid.',
                       'The tip of your weapon glows slightly...',
                       'After you have done this, you continue towards the light cautiously.',
                       'You near the light, and crouch behind a large rock.',
                       'You peer over slowly...']
        jFTimeList4 = [2.5, 2, 3, 3.5,
                       3, 2.5, 2,
                       2.5, 2.5, 3.5, 3, 2]
        jFTimeList4Fast = [1.5, 1, 2, 2,
                           2, 1.5, 1,
                           1.5, 1.5, 2, 2, 1]
        jFTimeList4Fastest = [.75, .5, 1, 1,
                              1, .75, .5,
                              .75, .75, 1, 1, .5]
        # jFTextList7 You feel heat(0) --- towards the light!(3)
        if userChoice3 == 1:
            health += 10
            for d in range(0, 4):
                print(jFTextList4[d])
                if speed == 1:
                    time.sleep(jFTimeList4[d])
                if speed == 2:
                    time.sleep(jFTimeList4Fast[d])
                if speed == 3:
                    time.sleep(jFTimeList4Fastest[d])
        # jFTextList7 You leave the vial(4) -- over slowly...(6)
        if userChoice3 == 2:
            for d in range(4, 7):
                print(jFTextList4[d])
                if speed == 1:
                    time.sleep(jFTimeList4[d])
                if speed == 2:
                    time.sleep(jFTimeList4Fast[d])
                if speed == 3:
                    time.sleep(jFTimeList4Fastest[d])
        # jFTextList7 You dip your weapon(7) -- over slowly...(11)
        if userChoice3 == 3:
            jorgonHealth += 10
            for d in range(7, 12):
                print(jFTextList4[d])
                if speed == 1:
                    time.sleep(jFTimeList4[d])
                if speed == 2:
                    time.sleep(jFTimeList4Fast[d])
                if speed == 3:
                    time.sleep(jFTimeList4Fastest[d])
    jFTextList5 = ['Suddenly, you are faced with a large beast!', 'You are stiff with shock!',
                   'The beast has a large snout-like face, but looks very much like a human...',
                   '"Hello human! I am Jorgon the Dog-Mouthed! A fierce warrior of the Dog People!',
                   '"Who are you?! So I might know the name of my meal?"',

                   'You prepare for battle!',
                   'You consider what to do...', '1.) Attempt to talk to the beast.',
                   '2.) Attack suddenly in an attempt to surprise the beast.', '3.) Brace yourself for a fight.']
    jFTimeList5 = [3, 1.5, 4.5, 5, 4, 1.5, 1.5, 1.5, 1.5, 1.5]
    jFTimeList5Fast = [2, .75, 2.5, 3, 2, .75, .75, .75, .75, .75]
    jFTimeList5Fastest = [1, .5, 1.5, 2, 1, .5, .5, .5, .5, .5]
    for e in range(0, 10):
        print(jFTextList5[e])
        if speed == 1:
            time.sleep(jFTimeList5[e])
        if speed == 2:
            time.sleep(jFTimeList5Fast[e])
        if speed == 3:
            time.sleep(jFTimeList5Fastest[e])
        if e == 4:
            print('You reply, "I am', userName, 'a fierce warrior of the... Human... People..."')
            time.sleep(tSpeed + 2)
            print('"Haha! Well,', userName, 'of the Human People, face my wraith!"')
            time.sleep(tSpeed + 2)
    userChoice4 = int(input(bcolors.blue + 'What would you like to do? ' + bcolors.end))
    time.sleep(2)
    if userChoice4 == 1:
        print('You consider what to say...')
        time.sleep(tSpeed + .5)
        print('1.) Threaten the beast.')
        time.sleep(1)
        print('2.) Make a deal with Jorgon.')
        time.sleep(1)
        userChoice5 = int(input(bcolors.blue + 'What will you say? ' + bcolors.end))
        time.sleep(1)
        if userChoice5 == 1:
            jFTextList6 = ['You yell at the beast, ', '"Ha! You mean to fight a mighty god such as me?!',
                           '"I will smite you if you do not let me pass!', '"Choose! Death or life beast!"',
                           'The beast gives a mighty laugh!',
                           'He says, "You expect me to be scared of you pitiful skinman?!',
                           'Prepare for battle!"']
            jFTimeList6 = [2, 3, 3, 2.5, 2.5, 4, 2]
            jFTimeList6Fast = [1, 2, 2, 1.5, 1.5, 2, 1]
            jFTimeList6Fastest = [.5, 1, 1, .75, .75, 1, .5]
            # jFTextList4 You yell at the beast(0) -- Prepare for battle(6)
            for f in range(0, 7):
                print(jFTextList6[f])
                if speed == 1:
                    time.sleep(jFTimeList6[f])
                if speed == 2:
                    time.sleep(jFTimeList6Fast[f])
                if speed == 3:
                    time.sleep(jFTimeList6Fastest[f])
        jFTextList7 = ['"Maybe we can make a deal so that we don\'t have to battle.", you say.',
                       'Jorgon laughs heartily.',
                       '"Fine! I will give you a chance you weakling!"',
                       'Jorgon then proceeds to pull out a knife and stabs himself in the stomach.']
        jFTimeList7 = [3, 2, 3, 4.5]
        jFTimeList7Fast = [2, 1, 2, 2.5]
        jFTimeList7Fastest = [1, .5, 1, 1]
        if userChoice5 == 2:
            jorgonHealth -= 15
            for g in range(0, 4):
                print(jFTextList7[g])
                if speed == 1:
                    time.sleep(jFTimeList7[g])
                if speed == 2:
                    time.sleep(jFTimeList7Fast[g])
                if speed == 3:
                    time.sleep(jFTimeList7Fastest[g])
    jFTextList8 = ['You nervously laugh.',
                   '"Would you like to hear a joke?" you say.',
                   '"What did the coffin say to the other sick coffin?"',
                   'You attack the beast, quickly dealing 10 damage.',
                   'Jorgon prepares to fight you.',]
    jFTimeList8 = [2, 3, 3, 2.5, 2]
    jFTimeList8Fast = [1, 2, 2, 1.5, 1]
    jFTimeList8Fastest = [.5, 1, 1, .75, .5]
    if userChoice4 == 2:
        jorgonHealth -= 10
        for h in range(0, 5):
            print(jFTextList8[h])
            if speed == 1:
                time.sleep(jFTimeList8[h])
            if speed == 2:
                time.sleep(jFTimeList8Fast[h])
            if speed == 3:
                time.sleep(jFTimeList8Fastest[h])
    if userChoice4 == 3:
        print('You prepare fight Jorgon the Dog-Mouthed.')
        time.sleep(tSpeed + 1)
    print(bcolors.purple + 'The Mighty Jorgon the Dog Mouthed' + bcolors.end)
    print('=======================')
    print('Base Damage:', jorgonBaseDamage)
    print('Attack Speed:', jorgonAttackSpeed)
    print('Armor: ', jorgonArmor)
    print('Resistance: ', jorgonResist)
    print('Health: ', jorgonHealth)
    print('=======================')
    print('Your attack can do', attackDamage, 'damage.')
    time.sleep(tSpeed + 1)
    print('Jorgon the Dog-Mouthed has', jorgonAttackDamage, 'Attack Damage and', jorgonHealth, 'health.')
    time.sleep(tSpeed + (3/speed))
    print('Jorgon the Dog-Mouthed quickly swipes at you with his mighty axe.')
    time.sleep(tSpeed + (2/speed))
    health -= jorgonAttackDamage
    print('He does', jorgonAttackDamage, 'damage to you.')
    time.sleep(tSpeed + (2/speed))
    print('You now have', health, 'health.')
    time.sleep(tSpeed + (2/speed))
    while jorgonLive:
        print('You consider what to do...')
        time.sleep(2)
        print('1.) Attack Jorgon.')
        time.sleep(1)
        print('2.) Run')
        time.sleep(1)
        print('3.) Do nothing')
        time.sleep(1)
        userChoice6 = int(input(bcolors.blue + 'What do you want to do? ' + bcolors.end))
        time.sleep(2)
        if userChoice6 == 1:
            jorgonHealth -= attackDamage
            print('You attack Jorgon with all your might!')
            time.sleep(tSpeed)
            print('You attack with', attackDamage, 'attack damage.')
            time.sleep(tSpeed)
            if jorgonHealth < 0:
                jorgonHealth = 0
            print('Jorgon now has', jorgonHealth, 'health!')
            time.sleep(tSpeed)
            if jorgonHealth > 0:
                print('Jorgon recovers himself and swings again with his mighty axe.')
                time.sleep(tSpeed)
                health -= jorgonAttackDamage
                print('Jorgon deals', jorgonAttackDamage, 'to you!')
                time.sleep(tSpeed)
                print('You now have', health, 'health.')
                time.sleep(tSpeed)
            if jorgonHealth <= 0:
                jorgonLive = False
        if jorgonLive != False:
            if userChoice6 == 2:
                jFTextList9 = ['You run from Jorgon the Dog-Mouthed.',
                               'You run down the cave as fast as you can, hearing howling behind you.',
                               'The beast is gaining on you...', 'He is close now..',
                               'You turn to see how close he is.',
                               'You trip and slam into the ground with a mighty force.',
                               'Before you can recover, Jorgon swings his might axe.', 'You have died.']
                jFTimeList9 = [2, 3, 2, 2, 2.5, 3, 5, 2]
                jFTimeList9Fast = [1, 2, 1, 1, 1.5, 2, 3, 1]
                jFTimeList9Fastest = [.5, 1, .5, .5, .75, 1, 1.5, .5]
                for i in range(0, 8):
                    print(jFTextList9[i])
                    if speed == 1:
                        time.sleep(jFTimeList9[i])
                    if speed == 2:
                        time.sleep(jFTimeList9Fast[i])
                    if speed == 3:
                        time.sleep(jFTimeList9Fastest[i])
                endGame()
            if userChoice6 == 3:
                print('You decide to do nothing.')
                time.sleep(tSpeed + 1)
                health -= jorgonAttackDamage
            print('Jorgon attack does', jorgonAttackDamage, 'damage to you.')
            time.sleep(tSpeed + .5)
            print('You now have', health, 'health.')
            time.sleep(tSpeed + .5)
            if jorgonHealth <= 0:
                jorgonLive = False
            if health <= 0:
                print('You have died by the hands of Jorgon the Dog-Mouthed.')
                time.sleep(tSpeed + 2)
                print('The End.')
                endGame()
    jFTextList10 = ['Jorgon has been slain!', 'You stand over his body, covered with blood.',
                    'You are rewarded with a choice of bonuses.', '1.) + 7 Health',
                    '2.) + 2 Magic Damage', '3.) + .25 Attack Speed']
    jFTimeList10 = [2, 2, 3, 3, 1, 1, 1]
    jFTimeList10Fast = [1, 1, 2, 2, .75, .75, .75]
    jFTimeList10Fastest = [.5, .5, 1, 1, .5, .5, .5]
    eDefeated += 1
    for j in range(0, 6):
        print(jFTextList10[j])
        if speed == 1:
            time.sleep(jFTimeList10[j])
        if speed == 2:
            time.sleep(jFTimeList10Fast[j])
        if speed == 3:
            time.sleep(jFTimeList10Fastest[j])
    bonusChoice = int(input(bcolors.blue + 'What bonus do you pick? ' + bcolors.end))
    time.sleep(1)
    if bonusChoice == 1:
        health += 7
        print('You now have', health, 'health.')
        time.sleep(2)
    if bonusChoice == 2:
        magicDamage += 2
        print('You now have', magicDamage, 'Magic Damage.')
        time.sleep(2)
        if userClass == 2:
            magicDamage += 1
            print('Your class bonus has been applied.')
            time.sleep(2)
    if bonusChoice == 3:
        attackSpeed += .25
        print('You now have', attackSpeed, 'Attack Speed.')
        time.sleep(2.5)
        if userClass == 3:
            attackSpeed += .25
            print('Your class bonus has been applied.')
            time.sleep(2)
    displayStats()
    luumFight()


def luumFight():
    # Battle of 'Luum the Slime Mold'
    global baseDamage
    global magicDamage
    global attackSpeed
    global armor
    global health
    global userName
    global eDefeated
    global weapon
    global speed
    global item
    # Luum the Slime Mold Stats
    sMoBaseDamage = 25
    sMoAttackSpeed = .5
    sMoArmor = 0
    sMoResist = 15
    sMoHealth = 70
    attackDamage = ((baseDamage + magicDamage) * attackSpeed) - sMoArmor - sMoResist
    sMoAttackDamage = ((sMoBaseDamage) * sMoAttackSpeed) - armor
    if speed == 1:
        tSpeed = 1.5
    if speed == 2:
        tSpeed = 1
    if speed == 3:
        tSpeed = .5
    print('You stare at the fallen body of Jorgon the Dog-Mouthed')
    time.sleep(2.5)
    luumFightTextList1 = ['Your axe is stuck in the it\'s chest, gruesomely...',
                          'You grip the handle and pull hard, pulling the axe from it\'s chest.',
                          'You clean the axe head off with your glove.', 'There is a dark scorch mark on it\'s chest.',
                          'You catch your breathe and sit down on the cave floor.', 'You take a swig from your canteen.',
                          'You know you must continue.', 'An arrow sticks out from the chest of the beast.',
                          'Break off the arrow  so you can retrieve the fletching.',
                          'You know it is time to push forward...']
    luumFightTimeList1 = [2, 2.5, 2, 2, 2.5, 1.5, 2, 2.5, 2, 2]
    luumFightTimeList1Fast = [1, 1.5, 1, 1, 1.5, .75, 1, 1.5, 1, 1]
    luumFightTimeList1Fastest = [.5, .75, .5, .5, .75, .5, .5, .75, .5, .5]
    if (userClass == 1):
        for x in range(0,3):
            print(luumFightTextList1[x])
            if speed == 1:
                time.sleep(luumFightTimeList1[x])
            if speed == 2:
                time.sleep(luumFightTimeList1Fast[x])
            if speed == 3:
                time.sleep(luumFightTimeList1Fastest[x])
    if (userClass == 2):
        for x in range(3, 7):
            print(luumFightTextList1[x])
            if speed == 1:
                time.sleep(luumFightTimeList1[x])
            if speed == 2:
                time.sleep(luumFightTimeList1Fast[x])
            if speed == 3:
                time.sleep(luumFightTimeList1Fastest[x])
    if (userClass == 3):
        for x in range(7,10):
            print(luumFightTextList1[x])
            if speed == 1:
                time.sleep(luumFightTimeList1[x])
            if speed == 2:
                time.sleep(luumFightTimeList1Fast[x])
            if speed == 3:
                time.sleep(luumFightTimeList1Fastest[x])
    print('You consider what to do...')
    time.sleep(1.5)
    print('1.) Light a torch to see the way.')
    time.sleep(1)
    print('2.) Travel in darkness.')
    time.sleep(1)
    userChoice1 = int(input(bcolors.blue + 'What do you want to do? ' + bcolors.end))
    time.sleep(1)
    luumFightTextList2 = ['You light a torch and hold it in front of your face.',
                          'You continue down the cave, traveling deeper and deeper.',
                          'You notice something on the ground ahead...', 'A dark spot...',
                          'It must be a pit of some sort...',
                          'As you draw closer you can see the full expanse of the pit.', 'You consider what to do...',
                          '1.) Look around for someway to cross it.', '2.) Go around the side of pit.',
                          '3.) Yell for help.',
                          'You decide to look for something to cross it.', 'You come across a long bridge.',
                          'It looks very dangerous, but you could probably make it...',
                          'You consider what to do...', '1.) Use the bridge.',
                          '2.) Turn back and try to go around the side of the pit.',
                          'You carefully navigate around the side of the pit.',
                          'Edging across, you hear rocks around you fall into the abyss.',
                          'The rocks are wet and slippery, you must be careful.',
                          'Suddenly, you feel the rocks breaking from under you!',
                          'You scramble to grab onto the ledge.',
                          'Your finger slips on the rocks!', 'You fall into the abyss!',
                          'You scream as you fall down!']
    luumFightTimeList2 = [2.5, 2.5, 2, 1, 2.5, 2.5, 1.5, 1.5, 1.5, 2, 1.5, 2, 1, 1.5, 1.5, 2, 2.5, 2, 2, 2, 1.5, 1.5,
                          1.5, 1.5]
    luumFightTimeList2Fast = [1.5, 1.5, 1, .5, 1.5, 1.5, .75, .75, .75, 1, .75, 1, .5, .75, .75, 1, 1.5, 1, 1, 1, .75,
                              .75, .75, .75]
    luumFightTimeList2Fastest = [.75, .75, .5, .5, .75, .75, .5, .5, .5, .5, .5, .5, .5, .5, .5, .5, .75, .5, .5, .5,
                                 .5, .5, .5, .5]
    if userChoice1 == 1:
        for y in range(0, 10):
            print(luumFightTextList2[y])
            if speed == 1:
                time.sleep(luumFightTimeList2[y])
            if speed == 2:
                time.sleep(luumFightTimeList2Fast[y])
            if speed == 3:
                time.sleep(luumFightTimeList2Fastest[y])
        userChoice2 = int(input(bcolors.blue + 'What do you want to do? ' + bcolors.end))
        if userChoice2 == 1:
            for y in range(11, 16):
                print(luumFightTextList2[y])
                if speed == 1:
                    time.sleep(luumFightTimeList2[y])
                if speed == 2:
                    time.sleep(luumFightTimeList2Fast[y])
                if speed == 3:
                    time.sleep(luumFightTimeList2Fastest[y])
            # This choice technically doesn't haven't specific choices, but will bring you forward.
            userChoice3 = int(input(bcolors.blue + 'What do you want to do? ' + bcolors.end))
            if userChoice3 == 1:
                # Character gets across the bridge and goes to the next battle.
                print('You make it safely across the bridge.')
                time.sleep(2)
                print(bcolors.red + 'This is the end of the game as of now.')
                time.sleep(2.5)
                print('For the sake of the experience, the game will continue as if you had not crossed.' + bcolors.end)
                time.sleep(4)
            if userChoice3 == 2:
                userChoice2 == 2
        if userChoice2 == 2:
            for y in range(16, 24):
                print(luumFightTextList2[y])
                if speed == 1:
                    time.sleep(luumFightTimeList2[y])
                if speed == 2:
                    time.sleep(luumFightTimeList2Fast[y])
                if speed == 3:
                    time.sleep(luumFightTimeList2Fastest[y])
    luumFightTextList3 = ['You decide to not light a torch in order not to draw attention to yourself.',
                          'It is nearly pitch black.', 'You stumble over a rock, and crash into the ground.',
                          'But you regain your stance, and steady yourself.', 'You continue walking...',
                          'Suddenly you fall into some sort of pit!',
                          'You yell into the darkness, "Hello? Anybody there?"',
                          'Your voice echoes through the cave.',
                          'You hear nothing but silence back...',
                          'Deciding it is best to move forward, you continue down the cave.',
                          'It is so dark, you can barely see. You use the side of the cave wall to navigate.',
                          'Suddenly, the rocks under you give way! You tumble down under a bombardment of debris.']
    luumFightTimeList3 = [2.5, 1, 2, 2, 1, 1.5, 2, 2, 2, 2.6, 3, 3]
    luumFightTimeList3Fast = [1.5, .5, 1, 1, .5, .75, 1, 1, 1, 1.5, 2, 2]
    luumFightTimeList3Fastest = [.75, .5, .5, .5, .5, .5, .5, .5, .5, .75, 1, 1]
    if userChoice1 == 2:
        for z in range(0, 6):
            print(luumFightTextList3[z])
            if speed == 1:
                time.sleep(luumFightTimeList3[z])
            if speed == 2:
                time.sleep(luumFightTimeList3Fast[z])
            if speed == 3:
                time.sleep(luumFightTimeList3Fastest[z])
        if z == 1:
            print('You follow the cave wall with your hand, still gripping your', weapon, '.')
            time.sleep(tSpeed + 1)
    if userChoice1 == 3:
        for z in range(6, 12):
            print(luumFightTextList3[z])
            time.sleep(luumFightTimeList3[z])
    print('You lie on the ground, dazed and confused.')
    if userChoice1 == 1:
        print('Your torch is nowhere to be seen.')
        time.sleep(1)
    luumFightTextList4 = ['Once you regain your breathe, you attempt to stand.',
                          'Your ears are ringing, but you can feel the ground vibrating under you...',
                          'The vibrating turns into a slow thump.',
                          'As your hearing comes back, you begin to hear something...',
                          'It\'s a something so disgusting you can\'t describe it...',
                          'It sounds... Slimy and wet.',
                          'SUDDENLY!',
                          'You see something coming from the darkness!',

                          'It appears to be a large block of green slime!',
                          'You see bones floating around in the slime, they appear to be human...',
                          'The block of slime is so large... ',
                          'You wonder what to do...']
    luumFightTimeList4 = [2, 3, 2, 2.5, 2.5, 1.5, 2, 2.5,
                          2, 2.5, 1.5, 1.5]
    luumFightTimeList4Fast = [1.5, 2, 1.5, 1.5, 1.5, 1, 1, 1.5,
                              1.5, 1.5, 1, 1]
    luumFightTimeList4Fastest = [1, 1, 1, 1, 1, .75, .75, 1,
                                 1, 1, .75, .75]
    for a in range(0, len(luumFightTextList4)):
        print(luumFightTextList4[a])
        if speed == 1:
            time.sleep(luumFightTimeList4[a])
        if speed == 2:
            time.sleep(luumFightTimeList4Fast[a])
        if speed == 3:
            time.sleep(luumFightTimeList4Fastest[a])
        if a == 7:
            for b in range(0,21):
                print()
                time.sleep(.25)
                if b == 10:
                    print(bcolors.purple + 'YOU FACE LUUM THE SLIME MOLD' + bcolors.end)
    userChoice3 = 0
    userChoice4 = 0
    userChoice5 = 0
    luumFightTextList5 = ['1.) Run up to the monster and swing your mighty axe!',
                          '2.) Attempt to communicate with the beast!',
                          '3.) Run!'

                          'You decide to launch yourself at the beast, yelling wildly!',
                          'You leap and swing your axe above your head.',
                          'Your axe sticks solidly into the Slime Mold!',
                          'You fall to the floor, and look up in confusion.',
                          'Your axe is dissolving in the Slime!',
                          'You lose half your base damage!',
                          'Luckily you have your backup retractable axe in your pocket!',
                          'You never leave without it!',
                          'This is going to be rough...'

                          '"Hello, Mr. Slime Thing! I was wondering if you could maybe..."',
                          '"I don\'t know... Die? Maybe?"',
                          '"Because that would be absolutely SUPER."',
                          '"That would really help a guy out."',
                          'The Slime Mold just sits there. And does nothing.',
                          '"I guess I might just want to kill you then, huh?"',
                          'You carefully approach the beast.']
    luumFightTimeList5 = [1, 1, 1,
                          2, 2, 2, 1.5, 1.5, 1.5, 2, 1.5,
                          2, 1.5, 1.5, 1.5, 2, 2, 1.5]
    luumFightTimeList5Fast = [1, 1, 1,
                              1, 1, 1, 1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1, 1, 1,]
    luumFightTimeList5Fastest = [.5, .5, .5,
                                 .75, .75, .75, .75, .75, .75, .75, .75,
                                  .75, .75, .75, .75, .75, .75, .75]
    # Warrior Choices
    if userClass == 1:
        for c in range(0, 3):
            print(luumFightTextList5[c])
            if speed == 1:
                time.sleep(luumFightTimeList5[c])
            if speed == 2:
                time.sleep(luumFightTimeList5Fast[c])
            if speed == 3:
                time.sleep(luumFightTimeList5Fastest[c])
        userChoice3 = int(input(bcolors.blue + 'What do you want to do? ' + bcolors.end))
        time.sleep(1)
        if userChoice3 == 1:
            baseDamage -= baseDamage/2
            for d in range(3, 12):
                print(luumFightTextList5[d])
                if speed == 1:
                    time.sleep(luumFightTimeList5[d])
                if speed == 2:
                    time.sleep(luumFightTimeList5Fast[d])
                if speed == 3:
                    time.sleep(luumFightTimeList5Fastest[d])
        if userChoice3 == 2:
            for e in range(12, 19):
                print(luumFightTextList5[e])
                if speed == 1:
                    time.sleep(luumFightTimeList5[e])
                if speed == 2:
                    time.sleep(luumFightTimeList5Fast[e])
                if speed == 3:
                    time.sleep(luumFightTimeList5Fastest[e])
    luumFightTextList6 = ['You choose to use a powerful ice spell!',
                          'You summon a mighty Ice Lance Spell, and cast it at the beast!',
                          'The Ice Lance hits the Slime Mold directly!',
                          'The Slime Mold gives out a anguished cry!',
                          'The ice begins to spread across the Slime Mold slowly.',
                          'Soon, the visible portion of the Slime Mold is covered in ice.',
                          'The Slime Mold is seemlingly immoblized!',

                          'You choose to use a powerful fire spell!',
                          'You summon a mighty fire lance, and cast it at the beast!',
                          'The fire lance strikes the Slime Mold directly!',
                          'The Slime Mold seems unfazed by your attack.',
                          'Now you have to face a Slime Mold that\'s on fire.',
                          'You made a poor choice...',
                          'The Slime Mold\'s Base Damage has increased.',

                          'You decide to attempt to talk the beast.',
                          '"Greetings mighty Slime Mold!"',
                          '"I ask not for much. Only safe passage, so neither of us may perish."',
                          '"For I know any battle with you would certainly end in my death."',
                          '"So what do you say?"']
    luumFightTimeList6 = [1.5, 2, 1.5,   1.5, 1.5, 2,   2.5, 1.5,
                          1.5, 2, 1.5,   1.5, 2, 1.5,   1.5,
                          1.5, 1.5, 3, 2.5, 1]
    luumFightTimeList6Fast = [1, 1.25, 1, 1, 1, 1.25, 1.5, 1,
                              1, 1.25, 1, 1, 1.25, 1, 1,
                              1, 1, 2, 1.5, 1]
    luumFightTimeList6Fastest = [1, 1, 1, 1, 1, 1, 1.25, 1,
                                 1, 1, 1, 1, 1, 1, 1,
                                 1, 1, 1.25, 1, 1,]
    # Mage Choices
    if userClass == 2:
        print('1.) Use a ice spell')
        time.sleep(1)
        print('2.) Use a fire spell')
        time.sleep(1)
        print('3.) Attempt to talk the beast.')
        time.sleep(1)
        print('4.) Run!')
        time.sleep(1)
        userChoice4 = int(input(bcolors.blue + 'What do you want to do? ' + bcolors.end))
        time.sleep(1)
        if userChoice4 == 1:
            for f in range(0, 7):
                print(luumFightTextList6[f])
                if speed == 1:
                    time.sleep(luumFightTimeList6[f])
                if speed == 2:
                    time.sleep(luumFightTimeList6Fast[f])
                if speed == 3:
                    time.sleep(luumFightTimeList6Fastest[f])
        if userChoice4 == 2:
            sMoBaseDamage *= 1.5
            for g in range(7, 14):
                print(luumFightTextList6[g])
                if speed == 1:
                    time.sleep(luumFightTimeList6[g])
                if speed == 2:
                    time.sleep(luumFightTimeList6Fast[g])
                if speed == 3:
                    time.sleep(luumFightTimeList6Fastest[g])
        if userChoice4 == 3:
            for g in range(14, 18):
                print(luumFightTextList6[g])
                if speed == 1:
                    time.sleep(luumFightTimeList6[g])
                if speed == 2:
                    time.sleep(luumFightTimeList6Fast[g])
                if speed == 3:
                    time.sleep(luumFightTimeList6Fastest[g])
                if g == 15:
                    print('"I am', userName, 'I wish to pass your cave!"')
                    time.sleep(2)
    luumFightTextList7 = ['You decide to launch a volley of arrows at your opponent!',
                          'In quick secession your arrows fly into the beast.',
                          'The beast groans in pain!', 'It takes 10 damage!',

                          'You have decided to light your arrows afire!',
                          'You fire several arrows at the beast!',
                          'The Slime Mold does not seem to mind very much.',
                          'The Slime Mold takes 5 damage.', ###
                          'But now you face a Slime Mold that is on fire...',
                          'The Slime Mold\'s Base Damage increases.',

                          'You decide to run for your worthless good-for-nothing life, just as a coward would.',
                          'You whip around and begin to run!',
                          'Your head slams into a solid rock wall.',
                          'You fall over and break your weak spine. So poetic.',
                          'The slime mold draws near to you.',
                          'It pauses and realizes such as weakling is not worth it\'s time.',
                          'You die of starvation, completely paralyzed from the neck down.']
    luumFightTimeList7 = [2, 2, 1.5, 1.5,
                          1.5, 1.5, 1.5, 1, 1.5, 2,
                          4, 1.5, 1.5,   1.5, 1.5, 2.5,   2.5]
    luumFightTimeList7Fast = [1, 1, 1, 1,
                              1, 1, 1, 1, 1, 1.25,
                              2.5, 1, 2, 1, 2, 1.5, 1.5]
    luumFightTimeList7Fastest = [.75, .75, .75, .75,
                                 .75, .75, .75, .75, .75, 1,
                                 1.5, 1, 1, 1, 1, 1, 1]
    # Hunter Choices
    if userClass == 3:
        print('1.) Fire a volley of arrows at the monster!')
        time.sleep(1)
        print('2.) Re-light your torch and light your arrows afire!')
        time.sleep(1)
        print('3.) Run!')
        time.sleep(1)
        userChoice5 = int(input(bcolors.blue + 'What do you want to do? ' + bcolors.end))
        time.sleep(1)
        if userChoice5 == 1:
            sMoHealth -= 10
            for h in range(0, 4):
                print(luumFightTextList7[h])
                if speed == 1:
                    time.sleep(luumFightTimeList7[h])
                if speed == 2:
                    time.sleep(luumFightTimeList7Fast[h])
                if speed == 3:
                    time.sleep(luumFightTimeList7Fastest[h])
            print('The Slime Mold now has', sMoHealth, 'health!')
            time.sleep(1.5)
        if userChoice5 == 2:
            sMoHealth -= 5
            sMoBaseDamage *= 1.25
            for i in range(4, 10):
                print(luumFightTextList7[i])
                if speed == 1:
                    time.sleep(luumFightTimeList7[i])
                if speed == 2:
                    time.sleep(luumFightTimeList7Fast[i])
                if speed == 3:
                    time.sleep(luumFightTimeList7Fastest[i])
                if i == 7:
                    print('The Slime Mold now has', sMoHealth, 'health!')
                    time.sleep(1.5)

    if userChoice3 == 3 or userChoice4 == 4 or userChoice5 == 3:
        for j in range(10, len(luumFightTextList7)):
            print(luumFightTextList7[j])
            if speed == 1:
                time.sleep(luumFightTimeList7[j])
            if speed == 2:
                time.sleep(luumFightTimeList7Fast[j])
            if speed == 3:
                time.sleep(luumFightTimeList7Fastest[j])
        endGame()
    # State the Slime Mold's Stats
    print(bcolors.purple + 'The Horrible Slime Mold' + bcolors.end)
    print('=======================')
    print('Base Damage:', sMoBaseDamage)
    print('Attack Speed:', sMoAttackSpeed)
    print('Armor: ', sMoArmor)
    print('Resistance: ', sMoResist)
    print('Health: ', sMoHealth)
    print('=======================')
    time.sleep(4)
    print('You consider what to do...' )
    time.sleep(1)
    print('1.) Attack the Slime Mold directly.')
    time.sleep(1)
    print('2.) Throw rocks at the Slime Mold')
    time.sleep(1)
    if userClass == 1:
        print('3.) Throw your axe at the Slime Mold')
        time.sleep(1)
    if userClass == 2 or userClass == 3:
        print('3.) Attack from afar')
        time.sleep(1)
    userChoice6 = int(input(bcolors.blue + 'What do you want to do? ' + bcolors.end))
    time.sleep(1)
    luumFightTextList8 = ['You decide to attack the Slime Mold directly!',
                          'You run at the monster!', ###
                          'The Slime Mold lets out a groan, echoing off the cave walls.',
                          'The Slime Mold trembles.',

                          'You decide to throw rocks at the Slime Mold.',
                          'Grabing rocks around, you throw a volley of rocks at the Slime Mold!',
                          'The Slime Mold can\'t move much',
                          'The Slime Mold takes 15 damage!',
                          'The Slime Mold doesn\'t seem like it\'s defending itself...',]
    luumFightTimeList8 = [1.5, 1, 2, 1.5,
                          1.5, 2, 1.5, 1, 2]
    luumFightTimeList8Fast = [1, 1, 1.25, 1,
                              1, 1.25, 1, 1, 1.25]
    luumFightTimeList8Fastest = [.75, .75, 1, .75,
                                 .75, 1, .75, .75, 1]
    if userChoice6 == 1:
        sMoHealth -= attackDamage
        for k in range(0, 4):
            print(luumFightTextList8[k])
            if speed == 1:
                time.sleep(luumFightTimeList8[k])
            if speed == 2:
                time.sleep(luumFightTimeList8Fast[k])
            if speed == 3:
                time.sleep(luumFightTimeList8Fastest[k])
            if k == 1:
                print('You attack doing', int(attackDamage), 'damage!')
                time.sleep(1.5)

    if userChoice6 == 2:
        sMoHealth -= 15
        for k in range(4, len(luumFightTextList8)):
            print(luumFightTextList8[k])
            if speed == 1:
                time.sleep(luumFightTimeList8[k])
            if speed == 2:
                time.sleep(luumFightTimeList8Fast[k])
            if speed == 3:
                time.sleep(luumFightTimeList8Fastest[k])
    if userChoice6 == 3:
        if userClass == 2 or userClass == 3:
            print('You decide to attack from afar!')
            time.sleep(1.5)
            print('You launch a volley of attacks at the beast!')
            time.sleep(2)
            if userClass == 2:
                print('Casting powerful spells in rapid succesion!')
                time.sleep(1.5)
            if userClass == 3:
                print('Firing arrows in rapid succesion!')
                time.sleep(1.5)
            print('The beast takes 30 damage from your attacks!')
            time.sleep(2)
            sMoHealth -= 30
    print('The Slime Mold now has', int(sMoHealth), 'health.')
    time.sleep(1.5)
    luumFightTextList9 = ['The Slime Mold does not seem to be fighting back...',
                          'This may be an easy fight...',
                          'Suddenly, the Slime Mold begins to...', ##
                          'The Slime Mold is beginning to grow taller!',
                          'Two arms burst out of its sides!',
                          'More body parts begin to form!',
                          'Before you stands a mighty giant! 40 meters tall!']
    luumFightTimeList9 = [2, 1.5, 3, 1.5, 1.5, 1.5, 2]
    luumFightTimeList9Fast = [1.25, 1, 3, 1, 1, 1, 1.25]
    luumFightTimeList9Fastest = [1, .75, 3, .75, .75, .75, 1]
    for l in range(0, len(luumFightTextList9)):
        print(luumFightTextList9[l])
        if speed == 1:
            time.sleep(luumFightTimeList9[l])
        if speed == 2:
            time.sleep(luumFightTimeList9Fast[l])
        if speed == 3:
            time.sleep(luumFightTimeList9Fastest[l])
        if l == 2:
            for x in range(0, 10):
                print(bcolors.red + 'CHANGE!')
                time.sleep(.05)
                print(bcolors.blue + 'CHANGE!')
                time.sleep(.05)
                print(bcolors.whiteGrey + 'CHANGE!')
                time.sleep(.05)
                print(bcolors.brightWhite + 'CHANGE!')
                time.sleep(.05)
                print(bcolors.greenYellow + 'CHANGE!')
                time.sleep(.05)
                print(bcolors.purple + 'CHANGE!')
                time.sleep(.05)
                print(bcolors.teal + 'CHANGE!')
                time.sleep(.05)
                print(bcolors.lightGrey + 'CHANGE!' + bcolors.end)
                time.sleep(.05)
    sMoBaseDamage += 10
    sMoAttackSpeed = 1
    sMoArmor = 10
    sMoResist = 1
    sMoHealth += 20
    print(bcolors.purple + 'The Slime Mold Giant!' + bcolors.end)
    print('=======================')
    print('Base Damage:', sMoBaseDamage)
    print('Attack Speed:', sMoAttackSpeed)
    print('Armor: ', sMoArmor)
    print('Resistance: ', sMoResist)
    print('Health: ', sMoHealth)
    print('=======================')
    time.sleep(6)
    luumFightTextList10 = ['"I am the Great and Powerful Slime Mold Giant of Axedorf!"',
                           '"My friends call me Doug!"',
                           '"Now face your demise weakling!"',
                           'The Slime Mold grabs a massive rock from the cave wall and wields it as a spear!',
                           'You shudder in fear!', 'You feverously consider what to do...',
                           '1.) Attempt to talk to the Giant!', '2.) Fearlessly attack!', '3.) Run for your dear life!',

                           'You decide to attempt to talk to the beast!',
                           '"Well Mr. Slime Mold Giant from Axedunk..."',
                           'The Slime Mold laughs heartily, "No but really, call me Doug!"',
                           '"Well, okay Doug. I just thought we got off on the the wrong foot..."',
                           '"The whole attacking thing was a whole misunderstanding. You seem like a super guy!"',
                           '"I think it would benifit each other if we both decided to head our different ways!"',
                           'The Slime Mold Giant pauses in thought...', '"So what do you say?"',
                           'The Slime Mold Giant laughs heartily once again!',
                           'He then launches the spear directly at you!', 'You take 35 damage!',
                           '"You think you could attack me like that and not face the consequences mortal?!"',
                           '"Any last words!? HAHAHAHAHAHAHA"',

                           'You decide to fearlessly attack!', 'Screaming, you run at the Slime Mold Giant!',
                           'With a quick swipe, the Slime Mold Giant hits you with his spear!',
                           'The hit slams you into the cave wall!', 'You hit your head against hard rock!',
                           'You take 35 damage!',
                           'Dazed, you barely have time to dodge the spear as the Slime Mold Giant lunges at you!',
                           '"Give up now mortal! You cannot face a mighty God such as I!"',

                           'You decide that you cannot face such a mighty warrior!',
                           'You turn to run!', 'The Slime Mold Giant bellows, "Not so fast mortal!"',
                           'The Slime Mold Giant lunges forward, and with a wide swipe he slams you into '
                           'the wall with his spear!', 'Your whole body crashes into the rock!',
                           'You take 30 damage!', '"The coward always dies first mortal! HAHAHAHA"',
                           '"Any last words for the spineless weakling?"']
    luumFightTimeList10 = [2.5, 2, 2, 3.5, 1.25, 1, 1, 1, 1,
                           1.5, 2, 3,   2.5, 3, 3,   1.5, 1.5, 2,   1.5, 1.5, 3,  2,
                           1.5, 2, 3,   1.5, 1.5, 1.25,   2.5, 2.5,
                           1, 1, 2,   4, 1.5, 1,   2, 1.5]
    luumFightTimeList10Fast = [1.5, 1, 1, 2.5, 1, .75, .75, .75, .75,
                               1, 1, 2,   1.5, 2, 2,   1, 1, 1,   1, 1, 2,  1.5,
                               1, 1, 2,  1, 1, 1,  2, 2,
                               1, 1, 1,   3, 1, 1,   1, 1]
    luumFightTimeList10Fastest = [1, .75, .75, 1.5, .75, .75, .75, .75, .75,
                                  .75, .75, 1,   1, 1, 1,   .75, .75, .75,   .75, .75, 1, 1,
                                  .75, .75, 1,   .75, .75, .75,   1, 1,
                                  .75, .75, .75,   2, .75, .75,   .75, .75]
    for m in range(0, 9):
        print(luumFightTextList10[m])
        if speed == 1:
            time.sleep(luumFightTimeList10[m])
        if speed == 2:
            time.sleep(luumFightTimeList10Fast[m])
        if speed == 3:
            time.sleep(luumFightTimeList10Fastest[m])
    userChoice7 = int(input(bcolors.blue + 'What shall you do?! ' + bcolors.end))
    time.sleep(1)
    if userChoice7 == 1:
        health -= 35
        for m in range(9, 22):
            print(luumFightTextList10[m])
            if speed == 1:
                time.sleep(luumFightTimeList10[m])
            if speed == 2:
                time.sleep(luumFightTimeList10Fast[m])
            if speed == 3:
                time.sleep(luumFightTimeList10Fastest[m])
    if userChoice7 == 2:
        health -= 35
        for m in range(22, 30):
            print(luumFightTextList10[m])
            if speed == 1:
                time.sleep(luumFightTimeList10[m])
            if speed == 2:
                time.sleep(luumFightTimeList10Fast[m])
            if speed == 3:
                time.sleep(luumFightTimeList10Fastest[m])
    if userChoice7 == 3:
        health -= 30
        for m in range(30, len(luumFightTextList10)):
            print(luumFightTextList10[m])
            if speed == 1:
                time.sleep(luumFightTimeList10[m])
            if speed == 2:
                time.sleep(luumFightTimeList10Fast[m])
            if speed == 3:
                time.sleep(luumFightTimeList10Fastest[m])
    print('The Slime Mold Giant raises his spear above his head, ready to attack!')
    time.sleep(2.5)
    print('Just as he is about to slay you, you hear a warrior cry from above!')
    time.sleep(2)
    luumFightTextList11 = ['He weilds a magical wooden spatula, blessed by the monks of High Temple!',
                           'His wooden spatula glows red as he strikes the Slime Mold Giant!',
                           'The Slime Mold Giant takes 35 damage!',
                           'The Slime Mold Giant screams in agony.',
                           '"I believe I left the oven on brb!" yells Greg, as a leaps 400m through the cave ceiling.',
                           'Both you and the Slime Mold Giant are stunned by what just happened...',
                           'You quickly consider what to do.', '1.) Use the element of suprise to attack!',
                           '2.) Use the element of Iron to attack!',
                           '3.) Try talking to the Slime Mold Giant about Chemistry Jokes',

                           'The Slime Mold Giant falls to the ground!',
                           'The rock spear shatters into thousands of pieces as it hits the cave floor!',
                           'You have defeated the mighty Slime Mold Giant!',
                           'You can choose from a list of bonuses:', '1.) + 15 health',
                           '2.) + 5 Base Damage', '3.) + 8 Armor']
    luumFightTimeList11 = [2.5, 2.5, 1.5,   1.5, 2.5, 2,   1, 1, 1,   1,
                           2, 2.25, 1.5,   1.5, 1, 1,   1]
    luumFightTimeList11Fast = [1.5, 1.5, 1,   1, 1.5, 1,   1, 1, 1,   1,
                               1, 1.5, 1,   1, 1, 1,   1]
    luumFightTimeList11Fastest = [1, 1, .75,   .75, 1, .75,   .75, .75, .75,   .75,
                                  .75, 1, .75,   .75, .75, .75,  .75]
    if item == 1 or item == 2 or item == 3:
        sMoHealth -= 35
        print('Suddenly you see Greg, your fellow', userClassText, 'leap into the put from above!')
        time.sleep(3)
        for n in range(0, 10):
            print(luumFightTextList11[n])
            if speed == 1:
                time.sleep(luumFightTimeList11[n])
            if speed == 2:
                time.sleep(luumFightTimeList11Fast[n])
            if speed == 3:
                time.sleep(luumFightTimeList11Fastest[n])
        userChoice8 = int(input(bcolors.blue + 'What do you wish to do? ' + bcolors.end))
        time.sleep(1)
        if userChoice8 == 1:
            print('You decide to use the element of suprise to attack the Slime Mold Giant!')
            time.sleep(2)
            print('You lunge at the stunned and damaged Slime Mold, dealing', int(attackDamage), 'damage!')
            time.sleep(2.5)
            sMoHealth -= attackDamage
        if userChoice8 == 2:
            print('You decide to attack the Slime Mold Giant with a powerful attack!')
            time.sleep(1.5)
            if userClass == 1:
                print('You raise your mighty axe above your head and swing at the Slime Mold Giant\'s leg!')
                time.sleep(2)
            if userClass == 2:
                print('You raise your staff above your head ready to summon a powerful spell!')
                time.sleep(2)
                print('Power flows through you as your cast a massive icicle at the Slime Mold Giant!')
                time.sleep(2.25)
            if userClass == 3:
                print('You draw back your bowstring as far as possible, releasing a arrow into the slime Mold!')
                time.sleep(2.25)
            print('You deal', int(attackDamage * 1.1), 'damage!')
            time.sleep(1.5)
            sMoHealth -= (attackDamage * 1.1)
        if userChoice8 == 3:
            print('You decide to talk to the Slime Mold Giant about Chemistry Jokes.')
            time.sleep(2)
            print('"Hey do you like Chemistry jokes?", you yell at the Slime Mold Giant.')
            time.sleep(2)
            print('The Slime Mold Giant lunges at you with his spear quickly dealing', int(sMoAttackDamage), 'damage.')
            time.sleep(2.5)
    print('The Slime Mold has', int(sMoHealth), 'health.')
    time.sleep(1.25)
    print('Your attack can do', int(attackDamage), 'damage.')
    time.sleep(1.25)
    while sMoHealth > 0 and health > 0:
        userChoice9 = 0
        print('You consider what to do.')
        time.sleep(1)
        print('1.) Attack the Slime Mold Giant with all your force!')
        time.sleep(1.25)
        print('2.) Prepare to dodge his next attack, dealing small damage!')
        time.sleep(1.25)
        print('3.) Do nothing')
        time.sleep(1.25)
        userChoice9 = int(input(bcolors.blue + 'What shall you do? ' + bcolors.end))
        time.sleep(1)
        if userChoice9 == 1:
            print('You decide to attack the Slime Mold Gaint with all your force!')
            time.sleep(2)
            print('You deal', int(attackDamage), 'damage to the Slime Mold Giant')
            time.sleep(2)
            sMoHealth -= attackDamage
            if sMoHealth > 0:
                print('The Slime Mold Giant launches his spear at you!')
                time.sleep(1.5)
                print('You take', int(sMoAttackDamage), 'damage!')
                time.sleep(1.5)
                health -= sMoAttackDamage
        if userChoice9 == 2:
            print('You prepare to dodge the rock spear!')
            time.sleep(1.5)
            print('The Slime Mold Giant lunges, you dodge and attack back!')
            time.sleep(1.5)
            print('You deal', int(attackDamage * .5), 'damage!')
            time.sleep(1.5)
            print('You take', int(sMoAttackDamage * .4), 'damage!')
            time.sleep(1.5)
            sMoHealth -= attackDamage * .5
            health -= sMoAttackDamage * .4
        if userChoice9 == 3:
            print('You decide to do nothing.')
            time.sleep(1.25)
            print('The Slime Mold Giant attacks dealing, ', int(sMoAttackDamage), 'damage!')
            time.sleep(1.75)
            health -= sMoAttackDamage
        if sMoHealth < 0:
            sMoHealth = 0
        if health < 0:
            health = 0
        print('The Slime Mold Giant has', int(sMoHealth), 'health.')
        time.sleep(1.5)
        print('You have', int(health), 'health.')
        time.sleep(1.5)
    if sMoHealth == 0:
        eDefeated += 1
        for n in range(10, len(luumFightTextList11)):
            print(luumFightTextList11[n])
            if speed == 1:
                time.sleep(luumFightTimeList11[n])
            if speed == 2:
                time.sleep(luumFightTimeList11Fast[n])
            if speed == 3:
                time.sleep(luumFightTimeList11Fastest[n])
        userChoice10 = int(input(bcolors.blue + 'What bonus do you want? ' + bcolors.end))
        time.sleep(1)
        if userChoice10 == 1:
            health += 15
            print('You have gained 15 health.')
            time.sleep(1)
        if userChoice10 == 2:
            baseDamage += 5
            print('You have gained 5 Base Damage.')
            time.sleep(1)
        if userChoice10 == 3:
            armor += 8
            print('You have gained 8 armor.')
            time.sleep(1)
            if userClass == 1:
                armor += 1
                print('Your class bonus has been applied')
                time.sleep(1)
        print('The game has finished.')
    if health == 0:
        print('The Slime Mold Giant stands before you, as it pushes the spear deep into your chest.')
        time.sleep(2.5)
        print('You have been killed by the Slime Mold Giant.')
        time.sleep(1.5)
        endGame()


def peyTonsofCash():
    # Fight after Luum the Slime Mold
    print('petTonsofCash has run')


def endGame():
    # Ends the game and displays the stats of the character before death.
    global userName
    global userClassText
    global eDefeated
    global baseDamage
    global magicDamage
    global attackSpeed
    global armor
    print('You have failed the game, and', userName, 'the', userClassText, 'is no more.')
    time.sleep(2.5)
    displayStats()
    print('You have defeated', eDefeated, 'enemies.')
    time.sleep(2)
    promptRetry()

def promptRetry():
    # Asks the player if they would like to try again.
    global baseDamage
    global magicDamage
    global attackSpeed
    global armor
    global userClass
    global userClassText
    global health
    global eDefeated
    print(bcolors.greenYellow + 'Choose a payment plan!')
    time.sleep(2)
    print('1.) $3.99 for one more life!')
    time.sleep(2)
    print('2.) $8.99 for two more lives!')
    time.sleep(2)
    print('3.) $28.99 for three more lives! [BEST VALUE]')
    time.sleep(2)
    print('4.) $48.99 for UNLIMITED POWER!' + bcolors.end)
    time.sleep(3)
    retryPrompt = input(bcolors.blue + 'Would you like to try again? ' + bcolors.end)
    if retryPrompt == 'Yes' or retryPrompt == 'yes' or retryPrompt == 'Y' or retryPrompt == 'y':
        userClass = 0
        userClassText = 'Null'
        baseDamage = 2
        magicDamage = 0
        attackSpeed = 1
        armor = 2
        health = 100
        eDefeated = 0
        cls()
        startGame()


def cls():
    for x in range(0, 20):
        print(' ')

mainMenu()